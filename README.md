## Pre commit jira hook

Hook for pre-commit (https://pre-commit.com/) that prepend jira ticket id to commit message.

Add to your .pre-commit-hooks.yaml
```
- repo: https://bitbucket.org/filip_ignasiak/pre-commit-jira
  rev: v0.0.8
  hooks:
    - id: pre-commit-jira
```