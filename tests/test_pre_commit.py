import pytest
from unittest import mock
from pre_commit_jira import get_ticket_id, prepend_ticket_id


@pytest.fixture
def test_commit_amend_file():
    return """XYZ-1: Commit msg

    # Please enter the commit message for your changes. Lines starting
    # with '#' will be ignored, and an empty message aborts the commit.
    """

@pytest.fixture
def test_new_commit_file():
    return """
    
    # Please enter the commit message for your changes. Lines starting
    # with '#' will be ignored, and an empty message aborts the commit.
    """

def test_new_commit(mocker, test_new_commit_file):
    mocked_data = mocker.mock_open(read_data=test_new_commit_file)
    builtin_open = "builtins.open"
    mocker.patch(builtin_open, mocked_data)

    prepend_ticket_id("test_file_path", "XYZ-1")
    mock.call().write("""XYZ-1:
    
    # Please enter the commit message for your changes. Lines starting
    # with '#' will be ignored, and an empty message aborts the commit.
    """) in mocked_data.mock_calls

def test_commit_amend(mocker, test_commit_amend_file):
    mocked_data = mocker.mock_open(read_data=test_commit_amend_file)
    builtin_open = "builtins.open"
    mocker.patch(builtin_open, mocked_data)

    prepend_ticket_id("test_file_path", "XYZ-1")
    mock.call().write("""XYZ-1: XYZ-1:
    
    # Please enter the commit message for your changes. Lines starting
    # with '#' will be ignored, and an empty message aborts the commit.
    """) not in mocked_data.mock_calls

def test_ticket_id():
    result = get_ticket_id('feature/XYZ-1-test-branch-name')

    assert result == 'XYZ-1'

def test_invalid_ticket_id():
    result = get_ticket_id('quick-fix-branch')

    assert not result
