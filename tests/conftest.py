import sys
import os

def pytest_sessionstart():
    sys.path.extend(
        [
            os.path.join(os.path.dirname(__file__), "../src/"),
        ]
    )