
#!/usr/bin/env python3

import argparse
import logging
import re
import subprocess


def get_ticket_id(branch):
    matches = re.findall('(feature|hotfix|bugfix)\\/(\\w+-\\d+)', branch)
    
    if matches:   
        return matches[0][1]


def prepend_ticket_id(file_path, ticket_id):
    with open(file_path, "r+") as file_handler:
        content = file_handler.read()
        # If prefix already exist, do not modify anything
        if f'{ticket_id}:' in content:
            return

        file_handler.seek(0, 0)
        file_handler.write(f"{ticket_id.upper()}: {content}")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("commit_msg_filepath", help='File path to commit message')

    args = parser.parse_args()
    
    try:
        branch = subprocess.check_output(
            ["git","symbolic-ref", "--short", "HEAD"], 
            universal_newlines=True
        ).strip()
    except Exception as e:
        print(e)

    ticket_id = get_ticket_id(branch)
    if not ticket_id:
        logging.warning('Invalid branch name. Commit msg unmodified.')
        return

    prepend_ticket_id(args.commit_msg_filepath, ticket_id)

if __name__ == "__main__":
    main()
